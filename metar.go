/*
Package metar gives you a data structure for metar informations.
*/
package metar

import (
	"fmt"
	"log"
	"regexp"
	"strconv"
	"strings"
	"time"
)

// Metar format with single fields
type Metar struct {
	Airport     string
	Time        time.Time
	Wind        *Wind
	Visibilty   string
	Events      string
	Clouds      string
	Temperature string
	DewPoint    string
	Qnh         string
}

// ParseMetar reads input string and try to parse it into a Metar object
func ParseMetar(metarString string) (metar *Metar, err error) {
	metarArray := strings.Fields(metarString)
	if len(metarArray) == 0 {
		err = fmt.Errorf("Input is empty")
		return
	}
	metar = &Metar{}
	metar.Airport = metarArray[0]
	metar.Time = parseDate(metarArray[1])
	metar.Wind = &Wind{Direction: metarArray[2][:3], Speed: metarArray[2][3:]}
	findAdditionalWindInfo(metarString, metar)
	metar.Temperature, metar.DewPoint = parseTemperature(metarString)
	metar.Qnh = parseQnh(metarString)
	return
}

func parseDate(metarDate string) time.Time {
	now := time.Now().UTC()
	enhancedDate := fmt.Sprintf("%s %s %d", now.Month().String()[:3], metarDate, now.Year())
	date, err := time.Parse("Jan 021504Z 2006", enhancedDate)
	if err != nil {
		fmt.Printf("Error parsing time %s\n", err)
	}
	if now.Day() < date.Day() {
		date = date.AddDate(0, 0, -1)
	}
	return date
}

func findAdditionalWindInfo(metarString string, metar *Metar) {
	variableExpr, _ := regexp.Compile("\\d{3}V\\d{3}")
	loc := variableExpr.FindStringIndex(metarString)
	if loc != nil {
		metar.Wind.AdditionalInfo = metarString[loc[0]:loc[1]]
	}

	gustExpr, _ := regexp.Compile("\\d{5}G\\d{2}(KT|MS)")
	loc = gustExpr.FindStringIndex(metarString)
	if loc != nil {
		metar.Wind.AdditionalInfo = metarString[loc[0]:loc[1]]
	}
}

func parseTemperature(metarString string) (temperature string, dewPoint string) {
	expr, _ := regexp.Compile("M?\\d{2}\\/M?\\d{2}")
	tempString := expr.FindString(metarString)
	temps := strings.Split(tempString, "/")
	temperature = temps[0]
	dewPoint = temps[1]
	return
}

func parseQnh(metarString string) string {
	expr, _ := regexp.Compile("Q[0-9]{3,4}")
	return expr.FindString(metarString)
}

// Wind consists of direction and speed
type Wind struct {
	Direction      string
	Speed          string
	AdditionalInfo string
}

// SpeedInKnots gives you the speed in knots
func (wind *Wind) SpeedInKnots() string {
	if strings.Contains(wind.Speed, "KT") {
		return wind.Speed
	}
	ms, _ := strconv.ParseFloat(strings.TrimSuffix(wind.Speed, "MPS"), 32)
	return fmt.Sprintf("%.0fKT", ms*1.944)
}

// SpeedInMeterPerSecond gives you the speed in meter per second
func (wind *Wind) SpeedInMeterPerSecond() string {
	if strings.Contains(wind.Speed, "MPS") {
		return wind.Speed
	}
	knots, err := strconv.ParseFloat(strings.TrimSuffix(wind.Speed, "KT"), 32)
	if err != nil {
		log.Printf("Error converting %s to a number", wind.Speed)
	}
	return fmt.Sprintf("%.0fMPS", knots/1.944)
}
