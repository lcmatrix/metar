package metar

import (
	"fmt"
	"testing"
	"time"
)

const SIMPLEMETAR = "EDDC 290800Z 22010KT CAVOK 05/M01 Q1013 NOSIG"
const VARIABLEWIND = "EDDC 290800Z 22010KT 180V250 CAVOK 05/M01 Q1013 NOSIG"
const GUSTWIND = "EDDC 290800Z 22010KT 23015G20KT CAVOK 05/M01 Q1013 NOSIG"
const COMPLEXMETAR = "EDDM 290850Z 33005KT 280V010 9999 -RA FEW004 BKN006 BKN048 16/15 Q1016 TEMPO 27008KT 4500 DZ="

func TestParseMetarOK(t *testing.T) {
	metar, err := ParseMetar(SIMPLEMETAR)
	if err != nil {
		t.Errorf("Unexpected error occured %v", err)
	}
	if metar.Airport != "EDDC" {
		t.Errorf("Wrong icao code. Expected EDDC but was %s", metar.Airport)
	}
	/*timezone, _ := time.LoadLocation("UTC")
	expectedTime := time.Date(2020, time.Now().Month(), 29, 8, 0, 0, 0, timezone)
	if metar.Time != expectedTime {
		t.Errorf("Wrong time. Expected %v but was %v", expectedTime, metar.Time)
	}*/
	expectedWind := Wind{Direction: "220", Speed: "10KT"}
	if metar.Wind.Direction != expectedWind.Direction || metar.Wind.Speed != expectedWind.Speed {
		t.Errorf("Wrong wind. Expected %v but was %v", expectedWind, metar.Wind)
	}
	expectedTemperature := "05"
	if expectedTemperature != metar.Temperature {
		t.Errorf("Wrond temperature. Expected %s but was %s", expectedTemperature, metar.Temperature)
	}
	expectedDewPoint := "M01"
	if expectedDewPoint != metar.DewPoint {
		t.Errorf("Wrond dew point. Expected %s but was %s", expectedDewPoint, metar.DewPoint)
	}
	expectedQnh := "Q1013"
	if expectedQnh != metar.Qnh {
		t.Errorf("Wrong Qnh. Expected %s but was %s", expectedQnh, metar.Qnh)
	}
}

func TestParseMetarAdditionalWindInformation(t *testing.T) {
	metar, err := ParseMetar(VARIABLEWIND)
	if err != nil {
		t.Errorf("Unexpected error occured %v", err)
	}
	expectedAdditionalWind := "180V250"
	if expectedAdditionalWind != metar.Wind.AdditionalInfo {
		t.Errorf("Wrong additional wind information. Expected %s but was %s", expectedAdditionalWind, metar.Wind.AdditionalInfo)
	}

	metar, err = ParseMetar(GUSTWIND)
	expectedGustWind := "23015G20KT"
	if expectedGustWind != metar.Wind.AdditionalInfo {
		t.Errorf("Wrong additional wind information. Expected %s but was %s", expectedGustWind, metar.Wind.AdditionalInfo)
	}
}

func TestParseMetarEmpty(t *testing.T) {
	metar, err := ParseMetar("")
	if err == nil {
		t.Errorf("Error expected but was nil")
	}

	if err.Error() != "Input is empty" {
		t.Errorf("Wrong error. Expected \"Input is empty\" but was %v", err)
	}

	if metar != nil {
		t.Errorf("Empty metar expected but was %v", metar)
	}
}

func TestParseDateCurrentDate(t *testing.T) {
	now := time.Now().UTC()
	timestamp := fmt.Sprintf("%0d1720Z", now.Day())
	actualDate := parseDate(timestamp)
	expectedDate := time.Date(now.Year(), now.Month(), now.Day(), 17, 20, 0, 0, time.UTC)
	if expectedDate != actualDate {
		t.Errorf("Wrong date. Expected %v but was %v", expectedDate, actualDate)
	}
}

func TestParseDateOneDayBehind(t *testing.T) {
	now := time.Now()
	metarDate := now.AddDate(0, 0, -1)
	timestamp := fmt.Sprintf("%d2350Z", metarDate.Day())
	actualDate := parseDate(timestamp)
	expectedDate := time.Date(metarDate.Year(), metarDate.Month(), metarDate.Day(), 23, 50, 0, 0, time.UTC)
	if actualDate != expectedDate {
		t.Errorf("Wrond date. Expected %v but was %v", expectedDate, actualDate)
	}
}

func TestParseDateYearChange(t *testing.T) {
	newYear := time.Date(2020, time.January, 1, 0, 1, 0, 0, time.UTC)
	newYearsEve := newYear.AddDate(0, 0, -1)
	expectedDate := time.Date(2019, time.December, 31, 0, 1, 0, 0, time.UTC)
	if newYearsEve != expectedDate {
		t.Errorf("Wrong date. Expected %v but was %v", expectedDate, newYearsEve)
	}
}

func TestSpeedInKnots(t *testing.T) {
	wind := &Wind{Direction: "200", Speed: "10KT"}
	actual := wind.SpeedInKnots()
	if actual != "10KT" {
		t.Errorf("Wrong wind speed. Expected 10KT but was %s", actual)
	}

	wind = &Wind{Direction: "200", Speed: "10MPS"}
	actual = wind.SpeedInKnots()
	if actual != "19KT" {
		t.Errorf("Wrong wind speed. Expected 19KT but was %s", actual)
	}
}

func TestSpeedInMeterPerSecond(t *testing.T) {
	wind := &Wind{Direction: "200", Speed: "10MPS"}
	actual := wind.SpeedInMeterPerSecond()
	if actual != "10MPS" {
		t.Errorf("Wrong wind speed. Expected 10MPS but was %s", actual)
	}

	wind = &Wind{Direction: "200", Speed: "10KT"}
	actual = wind.SpeedInMeterPerSecond()
	if actual != "5MPS" {
		t.Errorf("Wrong wind speed. Expected 5MPS but was %s", actual)
	}
}
